import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class ManageService {

    ip = "http://52.15.171.241:3004";

    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }

    loginUserDetails(): Observable<any> {
        return this.http.get(this.ip + "/specificadmindetails").map(res => res.json());
    }
    groupName(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/grpname").map(res => res.json());
    }
    updateAdmin(name, user, pwd, selectGroup): Observable<any> {
        return this.http.get(this.ip + "/adminprofileupdation?name=" + name + "&user_name=" + user + "&password=" + pwd + "&team_name=" + selectGroup).map(res => res.json());
    }


}
