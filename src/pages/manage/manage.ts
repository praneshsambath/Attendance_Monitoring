import { Component } from "@angular/core";
import { ManageService } from "./manage.service";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { LoginPage } from "../login/login";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";


@Component({

    selector: 'manage-settings',
    templateUrl: './manage.html',
    providers: [ManageService]
})
export class AppManage {
    user_details: any;
    before_split: String = "";
    after_split = [];
    name: any;
    user: any;
    pwd: any;
    group_name = [];
    group_name_copy = [];
    updated_data: any;
    details = [{}];
    selectGroup = [];

    visible: boolean = false;

    constructor(public loadingCtrl: LoadingController, private manageService: ManageService, private navctrl: NavController) {

        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: 'Loading....Please Wait',
            duration: 600
        });

        loading.present();

        this.manageService.loginUserDetails().subscribe(

            data => {
                this.user_details = data[0];

                this.selectGroup.push(this.user_details.admin_teams);
                console.log(this.selectGroup)
                this.name = this.user_details.admin_name;
                this.user = this.user_details.admin_username;
                this.pwd = this.user_details.admin_password;
                this.before_split = this.user_details.admin_teams;
                this.after_split = this.before_split.split(",");
                this.groupName(this.after_split);
            },
            err => console.log(err)
        )
    }
    groupName(value) {

        this.manageService.groupName().subscribe(

            data => {
                this.group_name = data;
                for (var i = 0; i < this.group_name.length; i++) {
                    this.group_name[i].status = false;
                    this.group_name_copy.push(this.group_name[i]);
                }
                this.team(value, this.group_name_copy);
            },
            err => console.log(err)
        )
    }
    team(value, value1) {

        for (var i = 0; i < value.length; i++) {
            for (var j = 0; j < value1.length; j++) {
                if (value[i] == value1[j].emp_team) {
                    value1[j].status = true;
                }
            }
        }
    }
    onEdit() {
        this.visible = true;
    }
    onUpdate(name, user, pwd, selectGroup) {
        this.manageService.updateAdmin(name, user, pwd, selectGroup).subscribe(
            data => {
                this.updated_data = data[0];
            },
            err => console.log(err)
        )
        alert("UPDATED SUCCESSFULLY");
        this.navctrl.push(LoginPage);
    }
}