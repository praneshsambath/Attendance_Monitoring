import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Tabs } from 'ionic-angular';
import { TabPresentDailyReport } from '../report/daily_details/tabs/present/present';
import { TabAbsentDailyReport } from '../report/daily_details/tabs/absent/absent';



@Component({
  selector: 'Present-Absent_tabs',
  templateUrl: 'prs_abs.html'
})
export class PresentAbsentTabs {

  header:any="REPORT";
  @ViewChild('myTabs') tabRef: Tabs;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidEnter() {
    this.tabRef.select(0);
  }

  tab1Root = TabPresentDailyReport;
  tab2Root = TabAbsentDailyReport;
}
