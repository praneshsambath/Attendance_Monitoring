import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Tabs } from 'ionic-angular';
import { AppDailyReport } from '../report/dailyreport/dailyreport';
import { AppMonthlyReport } from '../report/monthlyreport/monthlyreport';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  header:any="REPORT";
  @ViewChild('myTabs') tabRef: Tabs;


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }
  ionViewDidEnter() {
    this.tabRef.select(0);
  }

  tab1Root = AppDailyReport;
  tab3Root = AppMonthlyReport;
}
