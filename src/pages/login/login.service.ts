import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class LoginService {


    data: any
    ip = "http://52.15.171.241:3004";

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    checkUser(): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/admin").map(res => res.json());
    }

    sendUserData(name, pwd): Observable<any> {

        return this.http.get(this.ip + "/crisplogin?username=" + name + "&password=" + pwd).map(res => res.json());

    }
}