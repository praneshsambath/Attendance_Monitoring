import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from "../home/home";
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { LoginService } from './login.service';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';


@Component({

    selector: "login-page",
    templateUrl: 'login.html',
    providers: [LoginService]

})

export class LoginPage {

    user: any;
    valid: boolean = true;
    temp: any;
    login: any = { userName: "", psw: "" };
    

    ngOnInit() {
        this.LoginScreen();
    }

    constructor(public navCtrl: NavController,public loadingCtrl:LoadingController , public menu: MenuController, private loginService: LoginService) { }

    ionViewDidEnter() {
        this.menu.swipeEnable(false);
    }
    ionViewWillLeave() {
        this.menu.swipeEnable(true);
    }

    LoginScreen() {
        this.loginService.checkUser().subscribe(

            data => {
                this.user = data;
            
            },
            err => { console.log(err) }
        )
    }

    checkUser(login) {

        for (var i = 0; i < this.user.length; i++) {
            if ((login.userName == this.user[i].admin_username) && (login.psw == this.user[i].admin_password)) {
                this.temp = [this.user[i].admin_username, this.user[i]]; 
            
                localStorage.setItem('usrname_psw', JSON.stringify(this.temp));

                this.loginService.sendUserData(this.user[i].admin_username, this.user[i].admin_password).subscribe(

                    data => {
                        this.user = data;
         
                        localStorage.setItem('login_details', JSON.stringify(data));
                    },
                    err => { console.log(err) }
                )
                this.navCtrl.push(HomePage);
                this.valid = true;
                this.Loading();
            }
            else {
                this.valid = false;
            }
        }


    }

    Loading()
    {
        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: 'Loading....Please Wait',
            duration: 600
          });
      
          loading.present();
    }



}