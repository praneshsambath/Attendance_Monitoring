import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class HomeService {

    ip = "http://52.15.171.241:3004";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    time(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/lastupdatetime").map(res => res.json());
    }
    date(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/lastupdatedate").map(res => res.json());
    }

    present(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/present").map(res => res.json());
    }
    absent(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/absent").map(res => res.json());
    }
    total(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/total").map(res => res.json());
    }
    late(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/latecount").map(res => res.json());
    }
    teams(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/grpcount").map(res => res.json());
    }
    // fun(id): Observable<any> {
    //     alert("lksad");
    //     return this.http.get("http://192.168.1.190:3111/api/homepage/clientcheck?device_id="+id).map(res => res.json().timeout(3000,'Timeout has occurred.'));
    // }
}