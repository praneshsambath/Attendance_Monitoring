import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { HomeService } from './home.service';
import { Http } from "@angular/http";
import { AppCurrentPresent } from '../current_present/current_present';
import { AppCurrentAbsent } from '../current_absent/current_absent';
import { AppCurrentTeams } from '../current_teams/current_teams';
import { AppCurrentTotal } from '../current_total/current_total';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [HomeService]
})
export class HomePage {

  current_present: any;
  current_absent: any;
  total_emp: any;
  current_late: any;
  total_team: any;
  current_time: any;
  current_date: any;
  public toggled: boolean = false;
  dummy_storage: any;
  datas: any;
  new_var: any;
  storage=[];


  ngOnInit() {
    // this.fun();
    this.present();
    this.absent();
    this.total();
    this.late();
    this.teams();
    this.time();
    this.date();
  }

  constructor(public navCtrl: NavController, public menu: MenuController, private homeService: HomeService, public http: Http) { 

    this.storage = JSON.parse(localStorage.getItem('usrname_psw'));
    console.log(this.storage[1].admin_name,this.storage[1].admin_role)
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  time() {
    this.homeService.time().subscribe(

      data => {
        this.current_time = data[0].last_update_time;

      },
      err => console.log(err)
    )

  }
  date() {
    this.homeService.date().subscribe(

      data => {
        this.current_date = data[0].last_update_date;

      },
      err => console.log(err)
    )

  }
  present() {

    this.homeService.present().subscribe(

      data => {
        this.current_present = data[0].present;

      },
      err => console.log(err)
    )

  }
  presentNavigate() {
    this.navCtrl.push(AppCurrentPresent);
  }
  absent() {

    this.homeService.absent().subscribe(

      data => {
        this.current_absent = data[0].absent;
      },
      err => console.log(err)
    )
  }
  absentNavigate() {
    this.navCtrl.push(AppCurrentAbsent);
  }
  late() {

    this.homeService.late().subscribe(

      data => {
        this.current_late = data[0].latecount;

      },
      err => console.log(err)
    )
  }
  total() {


    this.homeService.total().subscribe(

      data => {
        this.total_emp = data[0].count;
      },

      err => console.log(err)

    )

  }
  totalNavigate() {
    this.navCtrl.push(AppCurrentTotal);
  }
  teams() {
    this.homeService.teams().subscribe(

      data => {
        this.total_team = data[0].group_count;
      },

      err => console.log(err)

    )
  }
  teamsNavigate() {
    this.navCtrl.push(AppCurrentTeams);
  }
}
