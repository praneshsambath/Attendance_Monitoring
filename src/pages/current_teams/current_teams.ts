import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CurrentTeamService } from "./current_teams.service";

@Component({

    selector: "current-teams",
    templateUrl: "./current_teams.html",
    providers: [CurrentTeamService]
})

export class AppCurrentTeams {

    teams_info = [];
    group_members = [];
    teams_info_copy = [];
    members: boolean = false;
    dummy_storage: any;
    username_password: any;
    datas: any;
    ngOnInit() {
        this.teams();
    }
    constructor(public navCtrl: NavController, private teamService: CurrentTeamService) {


    }

    teams() {


        this.dummy_storage = localStorage.getItem('login_details');

        this.datas = JSON.parse(this.dummy_storage);
       
        this.teams_info = this.datas;
  
        for (var i = 0; i < this.datas.length - 1; i++) {
            var a = {
                emp_team: this.datas[i].emp_team,
                isvisible: false
            }
            this.teams_info_copy.push(a);
        }
    }

    groupMembers(teamName) {

        for (var i = 0; i < this.teams_info_copy.length; i++) {
            if (teamName != this.teams_info_copy[i].emp_team) {

                this.teams_info_copy[i].isvisible = false;
            }
        }

        for (var a = 0; a < this.teams_info_copy.length; a++) {
            if (teamName == this.teams_info_copy[a].emp_team) {

                if (this.teams_info_copy[a].isvisible == false) {

                    this.teams_info_copy[a].isvisible = true;
                } else {

                    this.teams_info_copy[a].isvisible = false;
                }
            }
        }
        this.teamService.groupMembers(teamName).subscribe(

            data => {
                this.group_members = data;

            },
            err => console.log(err)
        )
    }
}