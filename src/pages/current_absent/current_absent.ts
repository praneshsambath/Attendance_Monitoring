import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CurrentAbsentService } from "./current_absent.service";


@Component({

    selector: "current-absent",
    templateUrl: "./current_absent.html",
    providers: [CurrentAbsentService]
})

export class AppCurrentAbsent {

    absent_employee = [];

    ngOnInit() {
        this.absentEmployee();
    }
    constructor(public navCtrl: NavController, private absentService: CurrentAbsentService) { }

    absentEmployee() {
        this.absentService.absentEmployee().subscribe(

            data => {
                this.absent_employee = data;
                console.log(data);
            },
            err => console.log(err)
        )
    }
}