import { Component } from "@angular/core";
import { MonthlyDetailService } from "./monthlt_details.service";



@Component({
    selector: 'monthly-details',
    templateUrl: 'monthly_details.html',
    providers: [MonthlyDetailService]
})

export class AppMonthlyDetails {
    dummy_storage: any;
    dates = [];
    emp_details = [];
    dates_copy = [];

    ngOnInit()
    {
        this.fetching();
    }

    constructor(private monthDetail: MonthlyDetailService) {

        
    }

    fetching()
    {
        this.dummy_storage = localStorage.getItem('weekly_date');
        console.log( this.dummy_storage)
        this.dates = JSON.parse(this.dummy_storage);


        for (var i = 0; i < this.dates.length; i++) {
            var a = {
                date: this.dates[i].DATE,
                isVisible: false
            }
            this.dates_copy.push(a);
        }   
    }

    sendDate(date) {

     
        for (var i = 0; i < this.dates_copy.length; i++) {
            if (date != this.dates_copy[i].date) {
                this.dates_copy[i].isVisible = false;
            }
        }
      
        for (var a = 0; a < this.dates_copy.length; a++) {
            if (date == this.dates_copy[a].date) {

                if (this.dates_copy[a].isVisible == false) {
                    this.dates_copy[a].isVisible = true;
                } else {
                    this.dates_copy[a].isVisible = false;
                }
            }
        }

        this.monthDetail.retrieveEmpDetails(date).subscribe(
            data => {
                this.emp_details = data;
                console.log( this.emp_details)
            },
            err => { console.log(err) }
        )
    }
}