import { Component } from "@angular/core";
import { DailyPresentService } from "./present.service";

@Component({

    selector: 'report-daily-present-tab',
    templateUrl: 'present.html',
    providers:[DailyPresentService]
})
export class TabPresentDailyReport {

    storage:any;
    date:any;
    present:any;

    constructor(public presentService:DailyPresentService)
    {
        
        this.storage = localStorage.getItem('send_date');

        this.date = JSON.parse(this.storage);

        this.presentService.presentDetails(this.date).subscribe(

            data => {
                this.present = data;
            },
            err => { console.log(err) }
        )
    }

}