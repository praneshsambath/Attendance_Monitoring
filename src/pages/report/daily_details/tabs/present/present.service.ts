import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class DailyPresentService {

    ip = "http://52.15.171.241:3004";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    presentDetails(date): Observable<any> {

        return this.http.get(this.ip + "/reportdailydetailpresent?date=" + date).map(res => res.json());
    }
}