import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class DailyAbsentService {

    ip = "http://52.15.171.241:3004";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    absentDetails(date): Observable<any> {

        return this.http.get(this.ip + "/reportdailydetailabsent?date=" + date).map(res => res.json());
    }
}