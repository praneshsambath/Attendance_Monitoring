import { Component } from "@angular/core";
import { DailyAbsentService } from "./absent.service";

@Component({

    selector: 'report-daily-absent-tab',
    templateUrl: 'absent.html',
    providers:[DailyAbsentService]
})
export class TabAbsentDailyReport {

    storage:any;
    date:any;
    absent:any;
    constructor(public absentService:DailyAbsentService)
    {
        
        this.storage = localStorage.getItem('send_date');

        this.date = JSON.parse(this.storage);

        this.absentService.absentDetails(this.date).subscribe(

            data => {
                this.absent = data;
            },
            err => { console.log(err) }
        )
    }

}