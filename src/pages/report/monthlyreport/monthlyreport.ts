import { Component } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { MonthlyService } from "./monthlyreport.service";
import { MenuController } from "ionic-angular/components/app/menu-controller";
import { AppMonthlyDetails } from "../monthly_details/monthly_details";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";


@Component({
    selector: "monthly-report",
    templateUrl: "/monthlyreport.html",
    providers: [MonthlyService]
})

export class AppMonthlyReport {
    myMonth_data = [];
    point: any;
    options = {};
    week_data = [];
    selected_monthYear: any;
    monthYear: any = new Date().toISOString();

    ngOnInit() {
        this.Loading();
        this.selected_monthYear = new Date().toISOString();
        this.monthlyStatus(this.selected_monthYear);
    }

    constructor(public loadingCtrl:LoadingController,public navControl: NavController, private menuctrl: MenuController, private monthservice: MonthlyService) { }
    ionViewWillEnter() {
        this.menuctrl.enable(true);
    }

    monthlyStatus(changed_month) {

        this.monthservice.monthlyStatus(changed_month).subscribe(

            data => {

                this.myMonth_data = data;

                this.chart();

            },
            err => { console.log(err) }
        )

    }
    chart() {
        this.options = {
            chart: { type: 'column', zoomType: 'x' },
            title: { text: '' },
            series: [{
                name: 'Present',
                data: [this.myMonth_data[0].present, this.myMonth_data[1].present1, this.myMonth_data[2].present2, this.myMonth_data[3].present3, this.myMonth_data[4].present4],
                allowPointSelect: true
            }, {
                name: 'Absent',
                data: [this.myMonth_data[0].absent, this.myMonth_data[1].absent1, this.myMonth_data[2].absent2, this.myMonth_data[3].absent3, this.myMonth_data[4].absent4],
                allowPointSelect: true
            }],
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['WEEK 1', 'WEEK 2', 'WEEK 3', 'WEEK 4', 'week 5']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },

        };
    }

    onPointSelect(e) {
        this.point = e.context.x + 1;

        this.monthservice.weekly(this.point).subscribe(

            data => {
                this.week_data = data;


                localStorage.setItem('weekly_date', JSON.stringify(data));
            },
            err => { console.log(err) }
        )

        this.navControl.push(AppMonthlyDetails);
    }
    Loading() {
        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: 'Loading....Please Wait',
            duration: 800
        });

        loading.present();
    }

}
