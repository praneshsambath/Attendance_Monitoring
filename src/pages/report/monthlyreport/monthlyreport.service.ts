import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class MonthlyService {

    ip = "http://52.15.171.241:3004";

    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    monthlyStatus(month): Observable<any> {
        console.log(this.ip + "/reportmonthly?date=" + month)
        return this.http.get(this.ip + "/reportmonthly?date=" + month).map(res => res.json());

    }
    weekly(week_num): Observable<any> {
      
        return this.http.get(this.ip + "/api/reportpage/weekdetail?week=" + week_num).map(res => res.json());
    }
}