import { Component } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { DailyService } from "./dailyreport.service";
import { MenuController } from "ionic-angular/components/app/menu-controller";
import { PresentAbsentTabs } from "../../present_absent_tabs/prs_abs";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";



@Component({
    selector: "daily-report",
    templateUrl: "/dailyreport.html",
    providers: [DailyService]
})

export class AppDailyReport {

    selected_date: any;
    point: any;
    options: any;
    date: any = new Date().toISOString();
    daily_count: any = {};
    ngOnInit() {
        this.Loading();
        this.selected_date = new Date().toISOString();
        this.dailyStatus(this.selected_date);

    }

    constructor(public loadingCtrl: LoadingController, public navControl: NavController, private dailyService: DailyService, private menuctrl: MenuController) { }


    ionViewWillEnter() {
        this.menuctrl.enable(true);
    }


    dailyStatus(changed_date) {
        let before_split = changed_date;
        let after_split = before_split.split("T");

        localStorage.setItem('send_date', JSON.stringify(after_split[0]));

        this.dailyService.dailyStatus(after_split[0]).subscribe(

            data => {

                this.daily_count = data[0];

                this.chart();
            },
            err => console.log(err)
        )
    }

    chart() {

        this.options = {
            chart: { type: 'column', zoomType: 'x' },
            title: { text: null },
            series: [{
                name: 'Present',
                data: [this.daily_count.present],
                allowPointSelect: true
            }, {
                name: 'Absent',
                data: [this.daily_count.absent],
                allowPointSelect: true
            }],
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['DAY']
            },
            yAxis: {
                title: {
                    text: 'COUNT'
                }
            },
        };
    }
    onPointSelect(e) {
        this.point = e.context.x + 1;

        this.navControl.push(PresentAbsentTabs);
    }
    Loading() {
        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: 'Loading....Please Wait',
            duration: 800
        });

        loading.present();
    }

}