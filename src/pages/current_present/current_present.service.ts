import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class CurrentPresentService {

    ip = "http://52.15.171.241:3004";

    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }

    presentEmployee(): Observable<any> {
        // alert()
        return this.http.get(this.ip + "/api/homepage/prdetail").map(res => res.json());
    }
    lateEmployee(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/latedetail").map(res => res.json());
    }


}