import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CurrentPresentService } from "./current_present.service";
@Component({

    selector: "current-present",
    templateUrl: "./current_present.html",
    providers: [CurrentPresentService]
})


export class AppCurrentPresent {

    present_employee: any = [];
    late_employee = [];
    final_array = [];


    ngOnInit() {
        this.presentEmployee();
    }
    constructor(public navCtrl: NavController, private presentService: CurrentPresentService) { }

    presentEmployee() {
        this.presentService.presentEmployee().subscribe(

            data => {
                this.present_employee = data;
                console.log(data);
                this.lateEmployee();
            },
            err => console.log(err)
        )

    }
    lateEmployee() {
        this.presentService.lateEmployee().subscribe(

            data => {
                this.late_employee = data;
                console.log(data);
                this.mergeArray();
            },
            err => console.log(err)
        )
    }

    mergeArray() {
        let array_length1 = this.present_employee.length;
        let array_length2 = this.late_employee.length;
        for (var i = 0; i < array_length1; i++) {
            this.final_array.push(this.present_employee[i])
        }
        for (var j = 0; j < array_length2; j++) {
            this.final_array.push(this.late_employee[j])
        }   
        console.log(this.final_array)
      
    }
}