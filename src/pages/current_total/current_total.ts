import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CurrentTotalService } from "./current_total.service";

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchFilter'
})
export class SearchFilter implements PipeTransform {
    status: boolean;
    transform(items: any[], criteria: any): any {

        return items.filter(item => {
            for (let key in item) {
                if (("" + item[key]).toLowerCase().indexOf(criteria.toLowerCase()) !== -1 || ("" + item[key]).includes(criteria) || ("" + item[key]).toLowerCase().includes(criteria)) {
                    return true;
                }
            }
            return false;
        });
    }
}
@Component({

    selector: "current-total",
    templateUrl: "./current_total.html",
    providers: [CurrentTotalService]
})

export class AppCurrentTotal {

    temp = [];
    name: string;
    public toggled: boolean = false;
    total_emp = [];
    permission_status :any;
    onDuty_status :any;

    ngOnInit() {
        this.totalEmployee();
    }
    constructor(public navCtrl: NavController, private totalService: CurrentTotalService) { }

    totalEmployee() {
        this.totalService.totalEmployee().subscribe(

            data => {
                this.total_emp = data;

                for (var i = 0; i < data.length; i++) {
                    this.temp.push(data[i].emp_name);
                }
            },
            err => console.log(err)
        )
    }

    permission(status1, value1) {
        this.totalService.permission_And_onDuty(status1, value1.emp_id).subscribe(

            data => { this.permission_status = data; 

                alert(this.permission_status.message);
                
                this.totalEmployee();
            },
            err => console.log(err)
        )


    }
    onDuty(status2, value2) {
        this.totalService.permission_And_onDuty(status2, value2.emp_id).subscribe(

            data => { this.onDuty_status = data;
                alert(this.onDuty_status.message);
                this.totalEmployee();
            },
            err => console.log(err)
        )

    }

}