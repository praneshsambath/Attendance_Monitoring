import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class CurrentTotalService {

    ip = "http://52.15.171.241:3004";

    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }

    totalEmployee(): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/todetail").map(res => res.json());
    }
    permission_And_onDuty(emp_status,emp_id): Observable<any> {
        console.log(emp_status,emp_id)
        console.log(this.ip + "/permissionorod?id=" + emp_id + "&status=" + emp_status)
        return this.http.get(this.ip + "/permissionorod?id=" + emp_id + "&status=" + emp_status).map(res =>res.json());
    }
}
