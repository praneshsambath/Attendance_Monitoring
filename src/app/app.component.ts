import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { AppManage } from '../pages/manage/manage';
import { AppBasicService } from './service';


@Component({
  templateUrl: 'app.html',
  providers: [AppBasicService]
})
export class MyApp {

  user_data = [];
  dummy_storage: any;
  datas: any;
  temp = [];
  profileName: any;
  role: any;

  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  ngOnInit() {
    this.sideMenu();
  }

  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(private appService: AppBasicService, public platform: Platform, public menu: MenuController, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    this.statusBar.backgroundColorByHexString('#488aff');

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: "home" },
      { title: 'Report', component: ListPage, icon: "stats" },
      { title: 'Manage', component: AppManage, icon: "hammer" },
      { title: 'LogOut', component: LoginPage, icon: "log-out" },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.show();
    });
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  sideMenu() {
    this.appService.sideMenu().subscribe(

      data => {
        this.user_data = data[3];
      },
      err => console.log(err)
    )
  }

}
