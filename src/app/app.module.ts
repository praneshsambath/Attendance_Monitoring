import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from "@ionic/storage";


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ChartModule } from 'angular2-highcharts';
import * as highcharts from 'highcharts';

// import { HomeService } from '../pages/home/home.service';
import { AppCurrentPresent } from '../pages/current_present/current_present';
// import { CurrentPresentService } from '../pages/current_present/current_present.service';
import { AppCurrentAbsent } from '../pages/current_absent/current_absent';
// import { CurrentAbsentService } from '../pages/current_absent/current_absent.service';
// import { CurrentLateService } from '../pages/current_late/current_late.service';
import { AppCurrentTeams } from '../pages/current_teams/current_teams';
// import { CurrentTeamService } from '../pages/current_teams/current_teams.service';
import { AppCurrentTotal } from '../pages/current_total/current_total';
import { SearchFilter } from '../pages/current_total/current_total';
 // import { CurrentTotalService } from '../pages/current_total/current_total.service';
import { AppDailyReport } from '../pages/report/dailyreport/dailyreport';

import { AppMonthlyReport } from '../pages/report/monthlyreport/monthlyreport';
// import { DailyService } from '../pages/report/dailyreport/dailyreport.service';
import { AppManage } from '../pages/manage/manage';
// import { MonthlyService } from '../pages/report/monthlyreport/monthlyreport.service';
// import { LoginService } from '../pages/login/login.service';

// import { AppBasicService } from './service';
import { AppMonthlyDetails } from '../pages/report/monthly_details/monthly_details';
// import { MonthlyDetailService } from '../pages/report/monthly_details/monthlt_details.service';

import { PresentAbsentTabs } from '../pages/present_absent_tabs/prs_abs';
import { TabAbsentDailyReport } from '../pages/report/daily_details/tabs/absent/absent';
import { TabPresentDailyReport } from '../pages/report/daily_details/tabs/present/present';




@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    ListPage,
    AppCurrentPresent,
    AppCurrentAbsent,
    AppCurrentTeams,
    AppCurrentTotal,
    AppDailyReport,
    AppMonthlyReport,
    AppManage,
    SearchFilter,
    AppMonthlyDetails,
    PresentAbsentTabs,
    TabAbsentDailyReport,
    TabPresentDailyReport

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, { tabsHideOnSubPages: true }), ChartModule.forRoot(highcharts),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    LoginPage,
    HomePage,
    ListPage,
    AppCurrentPresent,
    AppCurrentAbsent,
    AppCurrentTeams,
    AppCurrentTotal,
    AppDailyReport,
    AppMonthlyReport,
    AppManage,
    AppMonthlyDetails,

    PresentAbsentTabs,
    TabAbsentDailyReport,
    TabPresentDailyReport


  ],
  providers: [
    StatusBar,
    SplashScreen,HomePage,
    { provide: ErrorHandler, useClass: IonicErrorHandler },]
})
export class AppModule { }
